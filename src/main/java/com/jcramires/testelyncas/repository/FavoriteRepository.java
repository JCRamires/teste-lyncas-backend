package com.jcramires.testelyncas.repository;

import com.jcramires.testelyncas.entity.Favorite;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin("http://localhost:3000")
@RepositoryRestResource(collectionResourceRel = "favorites", path = "favorites")
public interface FavoriteRepository extends MongoRepository<Favorite, String> {
//    List<Favorite> findByName(@Param("name") String name);
}
