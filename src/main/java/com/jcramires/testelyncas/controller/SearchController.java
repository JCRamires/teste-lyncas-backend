package com.jcramires.testelyncas.controller;

import com.jcramires.testelyncas.model.SearchResult;
import com.jcramires.testelyncas.model.Volume;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@RestController
public class SearchController {

    private final RestTemplate restTemplate;

    public SearchController(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    @GetMapping("/bookSearch")
    public SearchResult searchBook(@RequestParam(value = "name") String bookName, @RequestParam(value = "startIndex", defaultValue = "0") String startIndex) {
        StringBuilder urlStringBuilder = new StringBuilder();
        urlStringBuilder.append("https://www.googleapis.com/books/v1/volumes?q=");
        urlStringBuilder.append(bookName);
        urlStringBuilder.append("&maxResults=20&printType=books");
        urlStringBuilder.append("&startIndex=");
        urlStringBuilder.append(startIndex);


        SearchResult searchResult = this.restTemplate.getForObject(urlStringBuilder.toString(), SearchResult.class);

        return searchResult;
    }

    @GetMapping("/book/{googleId}")
    public Volume getBookByGoogleId(@PathVariable String googleId) {
        StringBuilder urlStringBuilder = new StringBuilder();
        urlStringBuilder.append("https://www.googleapis.com/books/v1/volumes/");
        urlStringBuilder.append(googleId);

        Volume searchResult = this.restTemplate.getForObject(urlStringBuilder.toString(), Volume.class);

        return searchResult;
    }
}
